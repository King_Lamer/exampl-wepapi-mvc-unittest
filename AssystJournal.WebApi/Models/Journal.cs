﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AssystJournal.Models
{
    public class Journal
    {
        [DisplayName("Id")]
        [Key]
        public int Id { get; set; }

        [DisplayName("Дата")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }     

        [DisplayName("Обращение №")]
        public int AssystId { get; set; }

        [DisplayName("НК")]
        public int? ClientId { get; set; }

        [DisplayName("Описание")]
        public string Decription { get; set; }

        [DisplayName("Комментарии")]
        public virtual ICollection<Comment> Comments { get; set; }

        [DisplayName("Решение")]
        public string Result { get; set; }

        [DisplayName("Контрол")]
        public bool IsOnControl { get; set; }      

        public Journal()
        {
            Date = DateTime.Now;
        }
    }
}
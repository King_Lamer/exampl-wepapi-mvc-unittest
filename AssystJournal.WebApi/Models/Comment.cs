﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AssystJournal.Models
{
    public class Comment
    {
        [Key]
        public int Id { get; set; }

        [DisplayName("Дата")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy H:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }

        [DisplayName("Описание")]
        public string Text { get; set; }

        public virtual Journal Journal { get; set; }

        public Comment()
        {
            Date = DateTime.Now;
        }
    }
}
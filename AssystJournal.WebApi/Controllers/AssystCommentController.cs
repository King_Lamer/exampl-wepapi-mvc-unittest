﻿using AssystJournal.DAL;
using AssystJournal.Models;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;

namespace AssystJournal.WebApi.Controllers
{
    public class AssystCommentController : ApiController
    {
        private AssystJournalContext db = new AssystJournalContext();

        /// <summary>
        /// Получить все записи
        /// </summary>
        public IEnumerable<Comment> Get(FilteringComment filter, Paging paging)
        {
            if (paging == null)
                paging = new Paging();

            if (filter == null)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            return db.Comments.
                    NullSafeWhere(filter.GetCommentsForJournal()).
                    Skip((paging.Index - 1) * paging.Size).Take(paging.Size).
                    ToList();
        }     


        /// <summary>
        /// Добавить новую запись с комментарием
        /// </summary>
        public void Post(Comment comment)
        {
            Journal journalToUpdate = db.Journals.Find(comment.Journal.Id);
            journalToUpdate.Comments.Add(comment);
            comment.Journal = null;
            db.SaveChanges();
        }

        /// <summary>
        /// Удалить
        /// </summary>
        public void Delete(int id)
        {
            Comment comment = db.Comments.Find(id);
            int journalId = comment.Journal.Id;
            db.Comments.Remove(comment);
            db.SaveChanges();
        }

        /// <summary>
        /// Получить одну запись комментария
        /// </summary>
        public Comment Find(int id)
        {
            return db.Comments.Find(id);
        }

        /// <summary>
        /// Обновить запись
        /// </summary>
        public void Put(Comment comment)
        {
            if (comment == null)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            Comment commentToUpdate = db.Comments.Find(comment.Id);
            if (comment == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            db.Entry(commentToUpdate).CurrentValues.SetValues(comment);
            db.SaveChanges();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
﻿namespace AssystJournal.WebApi.Controllers
{
    public class Paging
    {
        public Paging()
        {
            Index = 1;
            Size = 20;
        }

        public Paging(int pageIndex, int pageSize)
        {
            Index = pageIndex;
            Size = pageSize;
        }

        public int Index { get; set; }
        public int Size { get; set; }
    }
}
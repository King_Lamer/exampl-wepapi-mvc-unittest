﻿using AssystJournal.Models;
using System;

namespace AssystJournal.WebApi.Controllers
{
    public class FilteringComment
    {
        private int _journalId;

        public FilteringComment(int journalId)
        {
            _journalId = journalId;
        }

        public Func<Comment, bool> GetCommentsForJournal()
        {
                return f => f.Journal.Id == _journalId;
        }
    }
}
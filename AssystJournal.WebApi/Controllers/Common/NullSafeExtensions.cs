﻿using AssystJournal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace AssystJournal.WebApi.Controllers
{
    public static class NullSafeExtensions
    {
       public static IEnumerable<Journal> NullSafeWhere(this IEnumerable<Journal> source,
            Func<Journal, bool> predicate)
        {
            return predicate == null ? source : source.Where(predicate);
        }

        public static IEnumerable<Comment> NullSafeWhere(this IEnumerable<Comment> source,
           Func<Comment, bool> predicate)
        {
            return predicate == null ? source : source.Where(predicate);
        }
    }
}
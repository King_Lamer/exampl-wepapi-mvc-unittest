﻿using AssystJournal.Models;
using System;

namespace AssystJournal.WebApi.Controllers
{
    public class FilteringJournal
    {
        private bool? _isOnControl;

        public FilteringJournal()
        {
            _isOnControl = null;
        }

        public FilteringJournal(bool? isOnControl)
        {
            _isOnControl = isOnControl;
        }

        public Func<Journal, bool> GetIsOnControlExpression()
        {
            if (_isOnControl != null)
                return f => f.IsOnControl == _isOnControl;
            else
                return null;
        }
    }
}
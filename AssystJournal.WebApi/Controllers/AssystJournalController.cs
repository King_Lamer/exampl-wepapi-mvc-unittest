﻿using AssystJournal.DAL;
using AssystJournal.Models;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;

namespace AssystJournal.WebApi.Controllers
{
    public class AssystJournalController : ApiController
    {
        private AssystJournalContext db = new AssystJournalContext();

        /// <summary>
        /// Получить все записи
        /// </summary>
        public IEnumerable<Journal> Get(FilteringJournal filter, Paging paging)
        {
            if (paging == null)
                paging = new Paging();

            if (filter == null)
                filter = new FilteringJournal();

            return db.Journals.
                    NullSafeWhere(filter.GetIsOnControlExpression()).
                    Skip((paging.Index - 1) * paging.Size).Take(paging.Size).
                    ToList();
        }     

        /// <summary>
        /// Получить одну запись из журнала обраещений по Id
        /// </summary>
        public Journal Find(int id)
        {
            return db.Journals.Find(id);
        }

        /// <summary>
        /// Добавить новую запись
        /// </summary>
        public void Post(Journal journal)
        {
            db.Journals.Add(journal);
            db.SaveChanges();
        }

        /// <summary>
        /// Удалить
        /// </summary>
        public void Delete(int id)
        {
            Journal journal = db.Journals.Find(id);
            db.Journals.Remove(journal);
            db.SaveChanges();
        }

        /// <summary>
        /// Обновить запись
        /// </summary>
        public void Put(Journal journal)
        {
            if (journal == null)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            Journal journalToUpdate = db.Journals.Find(journal.Id);
            if (journal == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            db.Entry(journalToUpdate).CurrentValues.SetValues(journal);
            db.SaveChanges();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
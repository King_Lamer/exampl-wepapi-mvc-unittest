﻿using AssystJournal.Models;
using System.Data.Entity;

namespace AssystJournal.DAL
{
    public class AssystJournalContext : DbContext
    {
        public AssystJournalContext()
           : base("DefaultConnection")
        {

        }

        public DbSet<Journal> Journals { get; set; }
        public DbSet<Comment> Comments { get; set; }
    }
}
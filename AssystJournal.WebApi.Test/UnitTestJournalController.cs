﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AssystJournal.WebApi.Controllers;
using System.Linq;

namespace AssystJournal.WebApi.Test
{
    [TestClass]
    public class UnitTestJournalController
    {
        [TestMethod]
        public void TestGetAllWithOutParams()
        {
            AssystJournalController assystJournal = new AssystJournalController();
            var items = assystJournal.Get(null, null);

            Assert.IsNotNull(items);
        }

        [TestMethod]
        public void TestGetAllWithParams()
        {
            AssystJournalController assystJournal = new AssystJournalController();
            var expected = assystJournal.Get(new FilteringJournal(isOnControl: true), new Paging(100000000,20));
            int actual = 0;

            Assert.IsNotNull(expected);
            Assert.AreEqual(expected.Count(), actual);
        }
    }
}

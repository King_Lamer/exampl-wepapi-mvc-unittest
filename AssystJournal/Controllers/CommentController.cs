﻿using AssystJournal.Models;
using AssystJournal.WebApi.Controllers;
using System.Net;
using System.Web.Mvc;

namespace AssystJournal.Controllers
{
    public class CommentController : Controller
    {
        private AssystCommentController assystComment = new AssystCommentController();

        [HttpGet]
        public ActionResult Create(int id)
        { 
            ViewBag.H2 = "Добавить комментарий";
            return View(new Comment() { Journal = new Journal() { Id = id } });
        }

        [HttpPost]
        public ActionResult Create(Comment comment)
        {
            ViewBag.H2 = "Добавить комментарий";
            assystComment.Post(comment);
            return RedirectToAction("Details", "Journal", new { id = comment.Journal.Id });
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            ViewBag.H2 = "Изменить комментарий";
            return View(assystComment.Find((int)id));
        }

        [HttpPost]
        public ActionResult Edit(Comment comment)
        {
            ViewBag.H2 = "Изменить комментарий";
            if (comment == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            assystComment.Put(comment);
            return RedirectToAction("Details", "Journal", new { id = comment.Journal.Id });
        }


        public ActionResult Delete(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            return View(assystComment.Find((int)id));
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id, int journalId)
        {
            assystComment.Delete(id);
            return RedirectToAction("Details", "Journal", new { id = journalId });   
        }      
    }
}
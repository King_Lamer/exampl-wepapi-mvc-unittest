﻿using AssystJournal.Models;
using AssystJournal.WebApi.Controllers;
using System;
using System.Net;
using System.Web.Mvc;

namespace AssystJournal.Controllers
{
    public class JournalController : Controller
    {
        private AssystJournalController assystJournal = new AssystJournalController();

        public ActionResult Index()
        {
            return View(assystJournal.Get(null, null));
        }

        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.H2 = "Добавить запись в журнал";
            var journal = new Journal();
                journal.Date = DateTime.Now;
            return View(journal);
        }

        public ActionResult ControlRequest()
        {
            ViewBag.Message = "Ваши обращения, которые вы контролируете";
            return View("Index", assystJournal.Get(new FilteringJournal(isOnControl: true), null));
        }

        [HttpPost]
        public ActionResult Create(Journal journal)
        {
            ViewBag.H2 = "Добавить запись в журнал";
            assystJournal.Post(journal);
            return Redirect("/");
        }
      
        public ActionResult Details(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            ViewBag.H2 = "Просмотр записи в журнале";
            return View(assystJournal.Find((int)id));
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            return View(assystJournal.Find((int)id));
        }

        [HttpPost]
        public ActionResult Edit(Journal journal)
        {
            ViewBag.H2 = "Изменить запись в журнале";

            if (journal == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            assystJournal.Put(journal);
            return Redirect("/");
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                      
            return View(assystJournal.Find((int)id));
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            assystJournal.Delete(id);
            return Redirect("/");
        }       
    }
}
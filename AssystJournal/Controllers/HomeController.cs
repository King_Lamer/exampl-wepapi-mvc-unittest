﻿using System.Web.Mvc;

namespace AssystJournal.Controllers
{
    public class HomeController : Controller
    { 
        public ActionResult About()
        {
            ViewBag.Message = "Краткая сводка по приложению";
            return View();
        }        
    }
}